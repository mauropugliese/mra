package tdd.training.mra;

import java.util.List;

public class MarsRover {
	
	private String[][] planet;
	private int marsRoverX;
	private int marsRoverY;
	private String facing;
	

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		// To be implemented
		this.marsRoverX= 0;
		this.marsRoverY= 0;
		this.facing= "N";
		
		
		planet= new String[planetX][planetY];
		
		for(int i=0;i<planetObstacles.size(); i++) {
			String t=planetObstacles.get(i);
			t=t.replace("(","");
			t=t.replace(")","");
		
			String[] split= t.split(",");
			int obstacleX=Integer.parseInt(split[0]);
			int obstacleY=Integer.parseInt(split[1]);
		
			planet[obstacleX][obstacleY]= "x";
		}
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		// To be implemented
		
		return planet[x][y]=="x";
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) {
		// To be implemented
		char commandChar;
		String command;
		
		if(commandString.isEmpty()){
			this.facing="N";
		
		}
		for (int i=0;i<commandString.length();i++) {
			 commandChar= commandString.charAt(i);
			 command = String.valueOf(commandChar);
		
		
			 if(command.equals("r")) {
				 this.facing="E";
			
			 }
			 else if(command.equals("l")) {
				 this.facing="W";
			
			 }
			 else if(command.equals("f") && facing=="N") {
				 this.marsRoverY= this.marsRoverY + 1;
			
			 }
		
			 else if(command.equals("b") && facing =="N") {
			
				 this.marsRoverY= this.marsRoverY - 1;
			
			 }
			 
			  if(command.equals("f") && facing=="E") {
				 this.marsRoverX= this.marsRoverX + 1;
			
			 }
		
			  else if(command.equals("b") && facing =="E") {
			
				 this.marsRoverX= this.marsRoverX - 1;
			
			 }
		}
		return "("+ marsRoverX+ ","+marsRoverY+ ","+facing +")";
	}

}
