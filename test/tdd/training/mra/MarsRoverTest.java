package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;


import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void createPlanetContainsObstacle() throws MarsRoverException {
		
		ArrayList<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(4,7)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(4, 7);
		
		assertEquals(true, obstacle);
	}
	
	@Test
	public void roverEmptyCommand() throws MarsRoverException {
		
		ArrayList<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		String commandString = "";
		String returnString = rover.executeCommand(commandString);
		
		assertEquals("(0,0,N)", returnString);
	}
	
	@Test
	public void roverTurnsRight() throws MarsRoverException {
		
		ArrayList<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		String commandString = "r";
		String returnString = rover.executeCommand(commandString);
		
		assertEquals("(0,0,E)", returnString);
	}
	
	@Test
	public void roverTurnsLeft() throws MarsRoverException {
		
		ArrayList<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		String commandString = "l";
		String returnString = rover.executeCommand(commandString);
		
		assertEquals("(0,0,W)", returnString);
	}
	
	@Test
	public void roverMovesForwardOfOneCell() throws MarsRoverException {
		
		ArrayList<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "f";
		String returnString = rover.executeCommand(commandString);
		
		assertEquals("(0,1,N)", returnString);
	}
	
	
	@Test
	public void roverMovesBackwardOfOneCell() throws MarsRoverException {
		
		ArrayList<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "f";
		String returnString = rover.executeCommand(commandString);
		commandString = "b";
		returnString = rover.executeCommand(commandString);	
		
		assertEquals("(0,0,N)", returnString);
	}
	
	@Test
	public void roverMovesCombined() throws MarsRoverException {
		
		ArrayList<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String commandString = "ffrff";
		String returnString = rover.executeCommand(commandString);
			
		
		assertEquals("(2,2,E)", returnString);
		
	}
	





}
